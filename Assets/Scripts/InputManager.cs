﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager Intance;
    public float _yaw = 0.0f;
    private float _pitch = 0.0f;

    private float _rollPC = 0.0f;

    private float _rollXboxL = 0.0f;
    private float _rollXboxR = 0.0f;
    private float _rollXboxCombined = 0.0f;

    private bool _isBoosting = false;

    private bool _isRebalance = false;

    private bool _isPCLastControl = true;
    private bool _isRestart = false;
    private bool _isNextLevel = false;
    private bool _isPrevLevel = false;
    private bool _isBraking = false;

    private bool _isExit = false;

    public InputData currentInputData;

    public InputData GetCurrentInputData()
    {
        currentInputData.isPCLastControl = _isPCLastControl;
        currentInputData.yaw = _yaw;
        currentInputData.pitch = _pitch;
        currentInputData.rollPC = _rollPC;
        currentInputData.rollXboxL = _rollXboxL;
        currentInputData.rollXboxR = _rollXboxR;
        currentInputData.rollXboxCombined = _rollXboxCombined;
        currentInputData.isBoosting = _isBoosting;
        currentInputData.isRebalance = _isRebalance;
        currentInputData.isRestart = _isRestart;
        currentInputData.isNextLevel = _isNextLevel;
        currentInputData.isPrevLevel = _isPrevLevel;
        currentInputData.isBraking = _isBraking;
        currentInputData.isExit = _isExit;

        return currentInputData;
    }

    void Awake()
    {
        if (Intance == null)
        {
            Intance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        _yaw = Input.GetAxis("Horizontal");
        _pitch = Input.GetAxis("Vertical");


        _rollPC = Input.GetAxis("RollPC");
        _isPCLastControl = true;

        _rollXboxL = Input.GetAxis("RollXboxLT");
        _rollXboxR = Input.GetAxis("RollXboxRT");
        _rollXboxCombined = _rollXboxR - _rollXboxL;
        _isPCLastControl = false;


        if (Input.GetButton("BoostPC") ||
            Input.GetButton("RebalancePC") ||
            Input.GetButton("RestartPC") ||
            Input.GetButton("NextPC") ||
            Input.GetButton("PrevPC") ||
            Input.GetButton("BrakePC"))
        {
            _isPCLastControl = true;
        }

        if (Input.GetButton("BoostXbox") ||
            Input.GetButton("RebalanceXbox") ||
            Input.GetButton("RestartXbox") ||
            Input.GetButton("NextXbox") ||
            Input.GetButton("PrevXbox") ||
            Input.GetButton("BrakeXbox"))
        {
            _isPCLastControl = false;
        }

        _isBoosting = (Input.GetButton("BoostPC") || Input.GetButton("BoostXbox"));
        _isRebalance = (Input.GetButton("RebalancePC") || Input.GetButton("RebalanceXbox"));

        _isRestart = (Input.GetButton("RestartPC") || Input.GetButton("RestartXbox"));
        _isNextLevel = (Input.GetButton("NextPC") || Input.GetButton("NextXbox"));
        _isPrevLevel = (Input.GetButton("PrevPC") || Input.GetButton("PrevXbox"));

        _isBraking = (Input.GetButton("BrakePC") || Input.GetButton("BrakeXbox"));
        _isExit = (Input.GetButton("ExitPC") || Input.GetButton("ExitXbox"));
    }
}

[System.Serializable]
public class InputData
{
    public bool isPCLastControl;

    [Range(-1, 1)] public float yaw = 0.0f;
    [Range(-1, 1)] public float pitch = 0.0f;
    [Range(-1, 1)] public float rollPC = 0.0f;

    [Range(0, 1)] public float rollXboxL = 0.0f;
    [Range(0, 1)] public float rollXboxR = 0.0f;
    [Range(-1, 1)] public float rollXboxCombined = 0.0f;

    public bool isBoosting = false;

    public bool isRebalance = false;
    public bool isRestart = false;
    public bool isNextLevel = false;
    public bool isPrevLevel = false;
    public bool isBraking = false;
    public bool isExit;
}