﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public GameObject mainMenu;
    public GameObject levelSelect;


    private void Start()
    {
        mainMenu.SetActive(true);
        levelSelect.SetActive(false);
    }

    public void Update()
    {

    }

    public void GoToLevelSelect()
    {
        mainMenu.SetActive(false);
        levelSelect.SetActive(true);
    }

    public void GoToMainMenu()
    {
        mainMenu.SetActive(true);
        levelSelect.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void PlayLevel(int i)
    {
        LevelLoadManager.Instance.LoadLevel(i);
    }

}
