﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetCollider : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ship" && this.gameObject.tag == "Earth")
        {
            //Win
            LevelLoadManager.Instance.NextLevel();
            Debug.Log("Win");
        }
        else if(collision.gameObject.tag == "Ship")
        {
            //Reset
            LevelLoadManager.Instance.ResetLevel();
            Debug.Log("Reset");
        }
        else
        {
            Destroy(collision.gameObject);
            //Destroy(Other);
        }
    }
}
