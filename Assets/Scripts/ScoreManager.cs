﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{

    public int score = 0;   // The player's score.
    public Text ScoreText;
    static private ScoreManager instance;
    static public ScoreManager Instance
    {
        get { return instance; }
    }

    public int pointsPerSec = 1;
    public GameObject Player;
    void Start()
    {
        if (instance == null)
        {
            // save this instance
            instance = this;
        }
        else
        {
            // more than one instance exists
            Debug.LogError(
                "More than one Scorekeeper exists in the scene.");
        }
        score = 0;
    }


    public void OnPickUp()
    {
        score += 1000;
    }



    void Update()
    {
        if (Player == null)
        {
            pointsPerSec = 0;
            score += pointsPerSec;
            
        }
        score += pointsPerSec;
        ScoreText.text = "Score: " + score;
    }
}
