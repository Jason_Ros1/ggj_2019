﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Planet : MonoBehaviour
{
    public float Mass
    {
        get { return _rb.mass; }
    }


    public const float G = 1;

    private Rigidbody _rb;
    public Attractable[] OribitingBodies;

    private void Start()
    {
        _rb = gameObject.GetComponent<Rigidbody>();
        _rb.mass = Mass;
        _rb.useGravity = false;

        foreach (var body in OribitingBodies)
        {
            float initV = Mathf.Sqrt(Mass / body.Position.magnitude);
            Vector3 perp = Vector3.Cross(transform.position - body.Position, Vector3.right - body.Position);
            body.AddForce(perp.normalized * initV);

            //Right is up
            //Down is Right
            //Left is Down
            //Up is right
        }
    }


    private Vector3 _direction;
    private float _distance;
    private float _forcemagnitude;

    public void CalculateGravityEffects(Attractable[] attractables)
    {
        foreach (var attractable in attractables)
        {
            if (attractable.gameObject == this.gameObject)
            {
                continue;
            }
            attractable.CurrentFrameGravityForces += CalculateGravityForce(attractable);
            attractable.GravityForcesAddedThisFrame.Add(CalculateGravityForce(attractable));
        }
    }

    private Vector3 CalculateGravityForce(Attractable attractable)
    {
        _direction = attractable.Position - transform.position;
        _distance = _direction.magnitude;

        _forcemagnitude = -(G * Mass * attractable.Mass) / (_distance * _distance);
        return _direction.normalized * _forcemagnitude;
    }

    public Vector3 CalculateGravityForceDeterministic(Attractable attractable, Vector3 FuturePosition)
    {
        Vector3 result;
        _direction = FuturePosition - transform.position;
        _distance = _direction.magnitude;

        _forcemagnitude = -(G * Mass * attractable.Mass) / (_distance * _distance);
        result = _direction.normalized * _forcemagnitude;
        return result;
    }
}