﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(LineRenderer))]
[RequireComponent(typeof(Rigidbody))]
public class PhysicsDetermisticLineRender : MonoBehaviour
{
    private LineRenderer LineRenderer;
    private Rigidbody rb;
    
    void Start()
    {
        LineRenderer = GetComponent<LineRenderer>();
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
    }
}
