﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(Rigidbody))]
public class Attractable : MonoBehaviour
{
    public Vector3 Position
    {
        get { return transform.position; }
    }

    [FormerlySerializedAs("GravityActive")]
    public bool UseSingleGravityActor;

    public Planet SingleGravityActor;
    public List<Vector3> GravityForcesAddedThisFrame = new List<Vector3>();
    public Vector3 CurrentFrameGravityForces;

    private Material LineRenderMat;

    private LineRenderer TrajectoryLine;
    private List<LineRenderer> LRs = new List<LineRenderer>();
    public AttractableManager AttractableManagerinst;


    public bool IsMiniMapController;

    public Transform MiniMapCamera;
    public float MiniMapDistance = 2;
    public float MiniMapLinewidth = 2;
    private LineRenderer TrajectoryLineMiniMap;


    private const int NUMBEROFPREDICTEDSTEPS = 100;
    private const float DT = 0.2f;

    public float Mass
    {
        get { return _rb.mass; }
    }


    private Rigidbody _rb;

    void Awake()
    {
        _rb = gameObject.GetComponent<Rigidbody>();
        _rb.useGravity = false;
        LineRenderMat = Resources.Load<Material>("LineRenderMat");
        TrajectoryLine = generateLineRenderer();
        TrajectoryLine.gameObject.layer = gameObject.layer;
        TrajectoryLine.startColor = Color.blue;
        TrajectoryLine.endColor = Color.blue;
        TrajectoryLine.startWidth = 0.1f;
        TrajectoryLine.endWidth = 0.1f;

        if (IsMiniMapController)
        {
            LineRenderMat = Resources.Load<Material>("LineRenderMat");
            TrajectoryLineMiniMap = generateLineRenderer();
            TrajectoryLineMiniMap.gameObject.layer = LayerMask.NameToLayer("minimap");
            TrajectoryLineMiniMap.startColor = Color.blue;
            TrajectoryLineMiniMap.endColor = Color.blue;
            TrajectoryLineMiniMap.startWidth = MiniMapLinewidth;
            TrajectoryLineMiniMap.endWidth = MiniMapLinewidth;
        }
    }

    public void AddForce(float x, float y, float z)
    {
        GravityForcesAddedThisFrame.Add(new Vector3(x, y, z));
        _rb.velocity += new Vector3(x, y, z);
    }

    public void AddForce(Vector3 vel)
    {
        GravityForcesAddedThisFrame.Add(vel);
        _rb.velocity += vel;
    }


    private const float LineRendererScaler = 3f;


    public void Step()
    {
        if (UseSingleGravityActor && SingleGravityActor != null)
        {
            SingleGravityActor.CalculateGravityEffects(new Attractable[1] {this});
        }

        int i = 0;
        _rb.AddForce(CurrentFrameGravityForces);

        Vector3[] trajectorysteps = new Vector3[NUMBEROFPREDICTEDSTEPS];
        Vector3 LastStepPosition = transform.position;
        Vector3 Velocity = _rb.velocity;

        trajectorysteps[0] = transform.position + _rb.velocity.normalized * 2;
        //Draw trajectory.
        for (int j = 1; j < NUMBEROFPREDICTEDSTEPS; j++)
        {
            Velocity += (AttractableManagerinst.CalculateGravityForceDeterministic(this,
                             LastStepPosition) * DT);
            //previous + velocity * dt
            trajectorysteps[j] = LastStepPosition + Velocity * DT;
            LastStepPosition = trajectorysteps[j];
        }

        TrajectoryLine.positionCount = NUMBEROFPREDICTEDSTEPS;
        TrajectoryLine.SetPositions(trajectorysteps);
        if (IsMiniMapController)
        {
            TrajectoryLineMiniMap.positionCount = NUMBEROFPREDICTEDSTEPS;
            TrajectoryLineMiniMap.SetPositions(trajectorysteps);
        }
        // Draw gravity Lines
        //foreach (var Force in GravityForcesAddedThisFrame)
        //{
        //    LineRenderer Renderer;
        //    if (i < LRs.Count)
        //    {
        //        Renderer = LRs[i];
        //    }
        //    else
        //    {
        //        Renderer = generateLineRenderer();
        //        LRs.Add(Renderer);
        //    }

        //    Renderer.SetPositions(new Vector3[2]
        //        {transform.position, transform.position + Force.normalized * LineRendererScaler});

        //    Renderer.startColor = new Color(Mathf.Clamp01(Force.magnitude / 5), 0, 0, 1);
        //    Renderer.endColor = new Color(Mathf.Clamp01(Force.magnitude / 5), 0, 0, 1);
        //    i++;
        //}

        // Manage any left over line renderers
        //while (i < LRs.Count)
        //{
        //    LineRenderer Garbage = LRs[i];
        //    LRs.Remove(Garbage);
        //    Destroy(Garbage);
        //    i++;
        //}

        GravityForcesAddedThisFrame.Clear();
        CurrentFrameGravityForces = Vector3.zero;

        if (IsMiniMapController)
        {
            MoveCamera();
        }
    }

    private void MoveCamera()
    {
        Transform Target = AttractableManagerinst.GetClosestPlanet(this);
        Vector3 perp = Vector3.Cross(transform.position - Target.position, _rb.velocity.normalized - Target.position);
        MiniMapCamera.position = Target.position + perp.normalized * MiniMapDistance;
        MiniMapCamera.LookAt(Target);
    }


    private LineRenderer generateLineRenderer()
    {
        LineRenderer Renderer;
        GameObject go = new GameObject();
        go.transform.position = transform.position;
        go.transform.parent = transform;
        Renderer = go.AddComponent<LineRenderer>();
        Renderer.material = LineRenderMat;
        Renderer.startWidth = .1f;
        Renderer.endWidth = .1f;
        return Renderer;
    }
}