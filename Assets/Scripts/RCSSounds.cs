﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RCSSounds : MonoBehaviour
{
    public bool isBoosterPlaying = false;

    public AudioSource RCSAudio;

    void Start()
    {
        RCSAudio.volume = 0;
    }

    public void PlayRCS()
    {
        Debug.Log("Play: BoosterSound");
        if (RCSAudio.volume != 1)
        {
            RCSAudio.volume = 1;
        }

    }

    public void StopRCS()
    {
        RCSAudio.volume = 0;
        Debug.Log("Stop: BoosterSound");
    }
}
