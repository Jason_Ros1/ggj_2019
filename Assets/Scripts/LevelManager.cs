﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    public Text levelText;


    // Start is called before the first frame update
    void Update()
    {
        if (LevelLoadManager.Instance.CurrentLevel != null)
        {
            levelText.text = "Level: " + LevelLoadManager.Instance.CurrentLevel.index;
        }
    }
}