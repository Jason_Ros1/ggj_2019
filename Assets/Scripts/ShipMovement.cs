﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ShipMovement : MonoBehaviour
{
    InputManager inputManager;

    public float yawSpeed = 50f;
    public float pitchSpeed = 50f;
    public float rollSpeed = 50f;
    public float resetShipSpeed = 0.1f;

    [Header("BoostFuel")] public float maxFuel = 100f;
    public float currentFuel;
    public float maxBurnRate = 10f;
    public float burnStep = 2f;
    public float decreaseStep = 4f;
    public float currentBurnStep = 0f;

    [Header("BOOSTPOWA")] public float boostPower = 500f;
    public float brakePower = 0.5f;

    Quaternion combinedRotation;

    Rigidbody rb;

    public bool resetShip = false;
    public bool isBoosting = false;
    public bool isAirBreaking = false;

    public ParticleSystem[] Boosters;
    public float ParticleSizeScaler = 2;

    public BoosterSounds boosterSounds;

    public float boostBurnScalar;

    public RCSSounds rcsSounds;

    // Start is called before the first frame update
    void Start()
    {
        inputManager = InputManager.Intance as InputManager;
        rb = this.gameObject.GetComponent<Rigidbody>();
        currentFuel = maxFuel;
        //boosterSounds = this.gameObject.GetComponent<BoosterSounds>();
    }

    // Update is called once per frame
    void Update()
    {
        InputData inputData = inputManager.GetCurrentInputData();

        RotateShip(inputData);

        BoostShip(inputData);

        AirBrakeShip(inputData);

        boostBurnScalar = currentBurnStep / maxBurnRate;

        if (boostBurnScalar > 0.05 && boostBurnScalar < 0.95)
        {
            if (!boosterSounds.isBoosterPlaying)
            {
                boosterSounds.PlayBooster();
                boosterSounds.isBoosterPlaying = true;
            }

            boosterSounds.PitchShift(boostBurnScalar);
        }
        else if (boostBurnScalar < 0.05)
        {
            if (boosterSounds.isBoosterPlaying)
            {
                boosterSounds.StopBooster();
                boosterSounds.isBoosterPlaying = false;
            }
        }
    }

    private void AirBrakeShip(InputData inputData)
    {
        isAirBreaking = inputData.isBraking;
    }

    private void BoostShip(InputData inputData)
    {
        isBoosting = inputData.isBoosting;
        if (inputData.isBoosting && currentFuel > 0)
        {
            if (currentBurnStep < maxBurnRate)
            {
                currentBurnStep += burnStep * Time.deltaTime;
            }
            else
            {
                currentBurnStep = maxBurnRate;
            }
        }
        else
        {
            if (currentBurnStep > 0)
            {
                currentBurnStep -= decreaseStep * Time.deltaTime;
            }
            else
            {
                currentBurnStep = 0f;
            }
        }

        foreach (var system in Boosters)
        {
            system.gameObject.SetActive(!(BurnRateMinMaxed < .2f));
            system.transform.localScale = new Vector3(BurnRateMinMaxed,
                system.transform.localScale.y,
                system.transform.localScale.z);
        }
    }

    private float BurnRateMinMaxed
    {
        get { return maxBurnRate + currentBurnStep - maxBurnRate; }
    }

    private void RotateShip(InputData inputData)
    {
        if (inputData.rollPC != 0 || inputData.rollXboxCombined != 0||
            inputData.yaw != 0 || inputData.pitch != 0)
        {
            rcsSounds.PlayRCS();
        }
        else
        {
            rcsSounds.StopRCS();
        }

        resetShip = inputData.isRebalance;
        float yawAngle = yawSpeed * inputData.yaw * Time.deltaTime;
        float pitchAngle = pitchSpeed * inputData.pitch * Time.deltaTime;

        float rollAngle;

        if (inputData.rollPC != 0)
        {
            rollAngle = rollSpeed * inputData.rollPC * Time.deltaTime;
        }
        else
        {
            rollAngle = rollSpeed * inputData.rollXboxCombined * Time.deltaTime;
        }

        //Debug.Log(inputData.ToString());

        combinedRotation = CalculateCombinedQuaternion(yawAngle, pitchAngle, rollAngle);
    }

    private Quaternion CalculateCombinedQuaternion(float yawAngle, float pitchAngle, float rollAngle)
    {
        Quaternion quatYaw = Quaternion.AngleAxis(yawAngle, Vector3.up);
        Quaternion quatPitch = Quaternion.AngleAxis(pitchAngle, Vector3.right);
        Quaternion quatRoll = Quaternion.AngleAxis(rollAngle, Vector3.back);

        return quatYaw * quatPitch * quatRoll;
    }

    void FixedUpdate()
    {
        ResetShip();
        BoostShipUpdate();
        AirBrakeUpdate();
    }

    private void AirBrakeUpdate()
    {
        if (isAirBreaking)
        {
            //rb.AddTorque(-rb.angularVelocity * resetShipSpeed);
            rb.AddForce(-rb.velocity * brakePower);
        }
    }

    private void BoostShipUpdate()
    {
        currentFuel -= currentBurnStep * Time.fixedDeltaTime;
        if (currentFuel > 0)
        {
            Vector3 boostDir = transform.forward * currentBurnStep * boostPower;
            rb.AddForce(boostDir * Time.fixedDeltaTime, ForceMode.Force);
        }



    }

    protected void ResetShip()
    {
        if (resetShip == false)
        {
            float angleInDegrees;
            Vector3 rotationAxis;
            combinedRotation.ToAngleAxis(out angleInDegrees, out rotationAxis);


            Vector3 angularDisplacement = rotationAxis * angleInDegrees * Mathf.Deg2Rad;
            Vector3 angularSpeed = angularDisplacement / Time.fixedDeltaTime;

            rb.AddRelativeTorque(angularSpeed);
        }
        else
        {
            //Debug.Log("hit" + rb.angularVelocity);
            rb.AddTorque(-rb.angularVelocity * resetShipSpeed);
        }
    }
}