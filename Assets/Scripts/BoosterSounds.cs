﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoosterSounds : MonoBehaviour
{
    public bool isBoosterPlaying = false;

    public AudioSource boosterAudio;

    public float pitchMin = -1;
    public float pitchMax = 3;

    void Start()
    {
        StopBooster();
    }

    public void PlayBooster()
    {
        Debug.Log("Play: BoosterSound");
        boosterAudio.volume = 1;
    }

    public void StopBooster()
    {
        boosterAudio.volume = 0;
        Debug.Log("Stop: BoosterSound");
    }

    public void PitchShift(float boostBurnScalar)
    {
        float range = pitchMax - pitchMin;
        boosterAudio.pitch =  1 - pitchMin + boostBurnScalar / (range *2);//(boostBurnScalar * range) - range/2;
        Debug.Log("Pitch Shifting");
        
    }
}
