﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoadManager : MonoBehaviour
{
    private InputManager inputManager;

    public List<levelinformation> Levels = new List<levelinformation>();
    public levelinformation CurrentLevel;
    public static LevelLoadManager Instance;

    private AudioSource Music;

    public AudioClip Tape;
    public AudioClip ComeHome;

    private bool playingComeHome;
    private bool playingTape;

    private bool inTransition = false;

    void Start()
    {
        if (Instance != null)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            Music = gameObject.AddComponent<AudioSource>();
            if (SceneManager.GetActiveScene().name != "menu")
                PlayIntroSceneaudio();
        }

        SceneManager.activeSceneChanged += (arg0, scene) =>
        {
            inTransition = false;
            if (SceneManager.GetActiveScene().name != "menu")
                PlayIntroSceneaudio();
        };

        inputManager = InputManager.Intance as InputManager;

        for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
        {
            levelinformation level = new levelinformation();
            string path = SceneUtility.GetScenePathByBuildIndex(i);
            string sceneName = path.Substring(0, path.Length - 6).Substring(path.LastIndexOf('/') + 1);
            level.name = sceneName;
            level.index = i;
            Levels.Add(level);
        }

        CurrentLevel = Levels[0];
    }

    private void PlayIntroSceneaudio()
    {
        AudioLoop.instance.Pause();
        Music.PlayOneShot(ComeHome);
        playingComeHome = true;
    }

    public void Update()
    {
        if (playingComeHome && !Music.isPlaying)
        {
            playingComeHome = false;
            Music.PlayOneShot(Tape);
            playingTape = true;
        }

        if (playingTape && !Music.isPlaying)
        {
            AudioLoop.instance.Resume();
            playingTape = false;
        }

        if (inputManager == null)
        {
            inputManager = InputManager.Intance as InputManager;
        }

        InputData inputData = inputManager.GetCurrentInputData();
        if (!inTransition)
        {
            if (inputData.isRestart)
            {
                ResetLevel();
            }

            if (inputData.isNextLevel)
            {
                NextLevel(true);
            }

            if (inputData.isPrevLevel)
            {
                PrevLevel();
            }
        }

        if (inputData.isExit)
        {
            LoadLevel(0);
        }
    }

    public void LoadLevel(int level)
    {
        inTransition = true;
        CurrentLevel = Levels[level];
        SceneManager.LoadScene(CurrentLevel.name);
    }

    private void LoadScene(string levelName)
    {
        inTransition = true;
        SceneManager.LoadScene(levelName);
    }

    public void ResetLevel()
    {
        inTransition = true;
        LoadScene(CurrentLevel.name);
    }

    public void ReturnToMenu()
    {
        inTransition = true;
        LoadScene("menu");
    }

    public void NextLevel(bool skip = false)
    {
        CurrentLevel.completeted = !skip;
        if (CurrentLevel.index + 1 >= Levels.Count())
        {
            ReturnToMenu();
        }
        else
        {
            CurrentLevel = Levels[CurrentLevel.index + 1];
            LoadScene(CurrentLevel.name);
        }
    }

    public void PrevLevel()
    {
        if (CurrentLevel.index - 1 > 0)
        {
        }
        else
        {
            CurrentLevel = Levels[CurrentLevel.index + 1];
        }

        LoadScene(CurrentLevel.name);
    }
}

public class levelinformation
{
    public string name;
    public int index;

    public bool completeted
    {
        get { return PlayerPrefs.GetInt(name + "completed", 0) == 1; }
        set { PlayerPrefs.SetInt(name + "completed", value ? 1 : 0); }
    }
}