﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AttractableManager : MonoBehaviour
{
    public Planet[] Planets;
    public Attractable[] Attractables;

    void Start()
    {
        Attractables = FindObjectsOfType<Attractable>();
        Planets = FindObjectsOfType<Planet>();
        foreach (var attractable in Attractables)
        {
            attractable.AttractableManagerinst = this;
        }
    }

    void FixedUpdate()
    {
        foreach (var planet in Planets)
        {
            if (planet.gameObject.activeInHierarchy)
                planet.CalculateGravityEffects(Attractables
                    .Where(attractable =>
                        attractable != null && !attractable.UseSingleGravityActor &&
                        attractable.gameObject.activeInHierarchy)
                    .ToArray());
        }

        foreach (var attractable in Attractables)
        {
            if (attractable == null)
            {
                continue;
            }

            if (attractable.gameObject.activeInHierarchy)
                attractable.Step();
        }
    }

    public Transform GetClosestPlanet(Attractable ship)
    {
        float dist = float.MaxValue;
        Transform result = null;
        foreach (var planet in Planets)
        {
            if (Vector3.Distance(planet.transform.position, ship.transform.position) < dist)
            {
                dist = Vector3.Distance(planet.transform.position, ship.transform.position);
                result = planet.transform;
            }
        }

        return result;
    }

    public Vector3 CalculateGravityForceDeterministic(Attractable a, Vector3 pos)
    {
        Vector3 result = Vector3.zero;
        foreach (var planet in Planets)
        {
            if (planet.gameObject == a.gameObject)
            {
                continue;
            }

            if (planet.gameObject.activeInHierarchy)
                result += planet.CalculateGravityForceDeterministic(a, pos);
        }

        return result;
    }
}