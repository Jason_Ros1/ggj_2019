﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FuelManager : MonoBehaviour
{
    public ShipMovement shipMovementScript;
    public Image bar;

    // Update is called once per frame
    void Update()
    {
        float localCurrentFuel = shipMovementScript.currentFuel;
        float localCurrentMaxFuel = shipMovementScript.maxFuel;

        float fuelScalar = localCurrentFuel / localCurrentMaxFuel;

        bar.fillAmount = fuelScalar;
    }

}
