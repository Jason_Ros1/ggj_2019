﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioLoop : MonoBehaviour
{
    public static AudioLoop instance;

    private AudioSource audio;

    private bool Paused;

    void Awake
        ()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            audio = GetComponent<AudioSource>();
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        if (!Paused)
            Resume();
    }

    public void Pause()
    {
        Paused = true;
        audio.Pause();
    }

    public void Resume()
    {
        Paused = false;
        audio.Play();
    }
}