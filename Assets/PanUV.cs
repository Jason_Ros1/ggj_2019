﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanUV : MonoBehaviour
{
    public AnimationCurve curve;
    private ParticleSystemRenderer ps;
    private Vector2 temp;
    private float time;

    void Start()
    {
        ps = GetComponent<ParticleSystemRenderer>();
    }

    void Update()
    {
        time += Time.deltaTime;
        if (time > 1) time = 0;
        temp.x = curve.Evaluate(time);
        ps.material.mainTextureOffset = temp;
    }
}